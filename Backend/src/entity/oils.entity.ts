import { User } from './user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from 'typeorm';

@Entity('oils')
export class Oils {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('nvarchar')
  title: string;
  

  @Column({ nullable:true })
  urlPoster: string | null;

  
  @Column({ default: false })
  isSold: boolean;


  @OneToMany(
    () => User,
    user => user.oils,
  )
  users: User[];

}
