import { UserRole } from '../enum/user.role';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { Watercolours } from './watercolour.entity';
import { Oils } from './oils.entity';
import { Drawings } from './drawings.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.Basic,
  })
  role: UserRole;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(
    () => Drawings,
    drawing=> drawing.users,
  )
  drawing: Drawings[];

  @OneToMany(
    () => Watercolours,
    watercolour=> watercolour.users,
  )
  watercolours: Watercolours[];

  @OneToMany(
    () => Oils,
    oil => oil.users,
  )
  oils: Oils[];


  @Column({ default: 0 })
  readingPoints: number;

  @Column({
    nullable: true,
  })
  banEndDate: Date;
}
