import { User } from './user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  
} from 'typeorm';

@Entity('watercolours')
export class Watercolours {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('nvarchar')
  title: string;
  

  @Column({ nullable:true })
  urlPoster: string | null;

  
  @Column({ default: false })
  isSold: boolean;


  @OneToMany(
    () => User,
    user => user.watercolours,
  )
  users: User[];

}
