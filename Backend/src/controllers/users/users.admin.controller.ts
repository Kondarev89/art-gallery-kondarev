import { UserDTO } from 'src/dtos/user.dto';
import { UsersService } from './../../services/users/users.services';
import {
  Controller,
  Delete,
  Param,
  Get,
  UseGuards,
  Query,
  Post,
  Body,
} from '@nestjs/common';
import { User } from 'src/entity/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enum/user.role';
import { WatercolourService} from 'src/services/paintings/watercolours.service';
import { CreatePaintingDTO } from 'src/dtos/create.painting.dto';
import { OilsService } from 'src/services/paintings/oils.service';
import { DrawingsService } from 'src/services/paintings/drawings.service';

@Controller('admin')
export class UsersAdminAController {
  constructor(
    private readonly userService: UsersService,
    private readonly drawingService: DrawingsService,
    private readonly watercoloursService: WatercolourService,
    private readonly oilsService: OilsService,
    
  ) {}

  @Get('users')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async getAll(): Promise<UserDTO[]> {
    return this.userService.getAllUsers();
  }

  @Get('users/:userId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async getUserById(@Param('userId') userId): Promise<User> {
    return this.userService.getUserById(userId);
  }

  @Delete('/users/:userId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async delete(@Param('userId') userId) {
    return await this.userService.delete(userId);
  }

  // @Get('/watercolours')
  // @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  // async allWatercolours(
  //   @Query('pageSize') pageSize: number,
  //   @Query('page') page: number,
  // ): Promise<SoldAvailablePaintingsDTO[]> {
  //   const allWatercolours = await this.watercoloursService.getAllWatercolours(pageSize, page);
  //   return allWatercolours;
  // }
  // @Get('/aquarels')
  // @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  // async allWatercolour(
  //   @Query('pageSize') pageSize: number,
  //   @Query('page') page: number,
  // ): Promise<SoldAvailablePaintingsDTO[]> {
  //   const allAquarels= await this.watercoloursService.getAllWatercolours(pageSize, page);
  //   return allAquarels;
  // }
  // @Get('/oils')
  // @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  // async allOils(
  //   @Query('pageSize') pageSize: number,
  //   @Query('page') page: number,
  // ): Promise<SoldAvailablePaintingsDTO[]> {
  //   const allOils = await this.oilsService.getAllOils(pageSize, page);
  //   return allOils;
  // }

  @Post('/watercolour')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async createWatercolours(@Body() watercolour: CreatePaintingDTO) {
    return this.watercoloursService.createWatercolour(watercolour.title);
  }
  @Post('/aquarel')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async createWatercolour(@Body() watercolour: CreatePaintingDTO) {
    return this.watercoloursService.createWatercolour(watercolour.title);
  }
  @Post('/oils')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async createOils(@Body() oil: CreatePaintingDTO) {
    return this.oilsService.createOil(oil.title);
  }

  @Delete('/watercolours/:watercoloursId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async deleteWatercolours(@Param('watercolourId') watercolourId) {
    return this.watercoloursService.deleteWatercolour(watercolourId);
  }

  @Delete('/aquarels/:aquarelsId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async deleteWatercolour(@Param('aquarelId') aquarelId) {
    return this.watercoloursService.deleteWatercolour(aquarelId);
  }

  @Delete('/oils/:oilsId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async deleteOils(@Param('watercolourId') oilsId) {
    return this.oilsService.deleteWatercolour(oilsId);
  }
}
