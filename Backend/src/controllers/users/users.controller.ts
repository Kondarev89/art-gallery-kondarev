import { User } from 'src/entity/user.entity';
import { UserDTO } from './../../dtos/user.dto';
import { UsersService } from './../../services/users/users.services';
import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UseGuards,
  Param,
  Get,
} from '@nestjs/common';
import { CreateUserDTO } from 'src/dtos/create.user.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enum/user.role';
import { TransformServices } from 'src/services/transform.services';

@Controller('users')
export class UsersController {
  constructor(
    private readonly userService: UsersService,
    private readonly transform: TransformServices,
  ) {}

  @Get('/:username')
  async getUserById(@Param('username') username): Promise<User[]> {
    
    return this.userService.getUserByIdBasic(username);
  }

  @Post()
  async register(
    // TOdO: Rename CreateUserDTO to UserDetailsDTO
    @Body(new ValidationPipe({ whitelist: true })) userDetails: CreateUserDTO,
  ): Promise<UserDTO> {
    const newUser = await this.userService.register(userDetails);
    return this.transform.transformUser(newUser);
  }
  @Post(':id/ban')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Administrator))
  async banUser(
    @Param('id') userId: string,
    @Body(new ValidationPipe({ whitelist: true })) banDto: any,
  ) {
    return await this.userService.banUser(+userId, banDto.period);
  }
}
