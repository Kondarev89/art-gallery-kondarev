import { UsersAdminAController } from './users/users.admin.controller';
import { UsersController } from './users/users.controller';
import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/service.module';
import { AuthController } from './users/auth.controller';
import { OilsController } from './paintings/oils.controller';
import { WatercolourController } from './paintings/watercolour.controler';
import { DrawingsController } from './paintings/drawings.controller';

@Module({
  imports: [ServicesModule],
  providers: [],
  controllers: [
    AuthController,
    UsersController,
    OilsController,
    WatercolourController,
    DrawingsController,
    UsersAdminAController,
  ],
})
export class ControllersModule {}
