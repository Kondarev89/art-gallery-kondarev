
import {
    Controller,
    Param,
    Get,
    Delete,
    Put,
    UseGuards,
    Query,
  } from '@nestjs/common';
  import { UserId } from 'src/auth/user-id.decorator';
  import { AuthGuard } from '@nestjs/passport';
import { WatercolourService } from 'src/services/paintings/watercolours.service';
import { Watercolours } from 'src/entity/watercolour.entity';
import { ReturnPaintingDTO } from 'src/dtos/return.painting.dto';
  
  @Controller('watercolours')
  export class WatercolourController {
    constructor(
      private readonly watercoloursService: WatercolourService,
    ) {}
    
    @Get()
    async allWatercolours(): Promise<ReturnPaintingDTO[]> {
      const allDrawings = await this.watercoloursService.getAllWatercolours();
      return allDrawings;
    }
  
    @Get(':id')
    // @UseGuards(AuthGuard('jwt'))
    async getWatercoloursById(@Param('id') id: number): Promise<Watercolours> {
      const watercolour = await this.watercoloursService.getWatercoloursById(id);
  
      return watercolour;
    }
  
  }
  