
import {
    Controller,
    Param,
    Get,
    Delete,
    Put,
    UseGuards,
    Query,
  } from '@nestjs/common';
  import { UserId } from 'src/auth/user-id.decorator';
  import { AuthGuard } from '@nestjs/passport';
import { DrawingsService } from 'src/services/paintings/drawings.service';
import { Drawings } from 'src/entity/drawings.entity';
import { ReturnPaintingDTO } from 'src/dtos/return.painting.dto';
  
  @Controller('drawings')
  export class DrawingsController {
    constructor(
      private readonly drawingService: DrawingsService,
    
    ) {}


    @Get()
    async allDrawings(): Promise<ReturnPaintingDTO[]> {
      const allDrawings = await this.drawingService.getAllDrawings();
      return allDrawings;
    }
  
    @Get(':id')
    // @UseGuards(AuthGuard('jwt'))
    async getDrawingsById(@Param('id') id: number): Promise<Drawings> {
      const watercolour = await this.drawingService.getDrawingById(id);
  
      return watercolour;
    }
  }
  