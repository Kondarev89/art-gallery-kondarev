import {
  Controller,
  Param,
  Get,
  Delete,
  Put,
  UseGuards,
  Query,
} from '@nestjs/common';
import { UserId } from 'src/auth/user-id.decorator';
import { AuthGuard } from '@nestjs/passport';
import { OilsService } from 'src/services/paintings/oils.service';
import { Oils } from 'src/entity/oils.entity';
import { ReturnPaintingDTO } from 'src/dtos/return.painting.dto';

@Controller('oils')
export class OilsController {
  constructor(
    private readonly oilService: OilsService,
  ) {}
  

  
  @Get()
  async allOils(): Promise<ReturnPaintingDTO[]> {
    const oils = await this.oilService.getAllOils();
    return oils;
  }



  @Get(':id')
  // @UseGuards(AuthGuard('jwt'))
  async getOilsById(@Param('id') id: number): Promise<Oils> {
    const painting = await this.oilService.getOilsById(id);

    return painting;
  }

}
