import { IsNotEmpty, IsString } from "class-validator";

export class CreatePaintingDTO {
    @IsNotEmpty()
    @IsString()
    id: number;
    @IsNotEmpty()
    @IsString()
    title: string;

    // urlPoster: string | null;
}
