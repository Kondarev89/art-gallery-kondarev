import { UserDTO } from 'src/dtos/user.dto';
import { TransformServices } from 'src/services/transform.services';
import { User } from '../../entity/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { CreateUserDTO } from 'src/dtos/create.user.dto';
@Injectable()
export class UsersService {
  private static readonly PASSWORD_ENCRYPTION_SALT = 10;

  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly transform: TransformServices,
  ) {}

  async getAllUsers() {
    const allUser = await this.usersRepository.find();
    return allUser.map((e)=> this.transform.transformUser(e));
    //return this.transform.transformBooks(books);
  }

  async getUserByIdBasic(username: string) {
    const user = await this.usersRepository.find({ where: { username: username} });
    console.log(user)
    return user;
  }
  async getUserById(userId: number): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: {
        id: userId,
      },
      relations: ['borrowedBooks'],
    });
    return user;
  }

  async register(userDetails: CreateUserDTO): Promise<User> {
    // TODO (optional): check if user with that username already exists
    const newUser = this.usersRepository.create(userDetails);

    newUser.password = await bcrypt.hash(
      newUser.password,
      // UsersService.PASSWORD_ENCRYPTION_SALT,
      10
      ////i made changes...
    );
     const createdUser = await this.usersRepository.save(newUser);
     return createdUser;
  }

  async delete(userId: number): Promise<UserDTO> {
    const foundUser = await this.usersRepository.findOne(userId);
    foundUser.isDeleted = true;
    const deletedUser = await this.usersRepository.save(foundUser);
    return this.transform.transformUser(deletedUser);
  }

  async banUser(userId: number, period: number) {
    const user = await this.findOneOrFail(userId);
    user.banEndDate = new Date(Date.now() + period);

    return await this.usersRepository.save(user);
  }
  private async findOneOrFail(userId: number): Promise<User> {
    const user = await this.usersRepository.findOne(userId);
    if (!user) {
      throw new Error('No user!');
    }

    return user;
  }
}
