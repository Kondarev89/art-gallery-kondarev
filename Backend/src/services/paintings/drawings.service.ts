import { User } from '../../entity/user.entity';
import {
  Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Drawings } from 'src/entity/drawings.entity';
import { ReturnPaintingDTO } from 'src/dtos/return.painting.dto';

@Injectable()
export class DrawingsService {

  constructor(

    @InjectRepository(Drawings) private readonly drawingRepository: Repository<Drawings>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}


  async getAllDrawings(): Promise<ReturnPaintingDTO[]> {
    const drawings = await this.drawingRepository.find();
    return drawings;
  }

  async getDrawingById(id: number): Promise<Drawings> {
    const drawing = await this.drawingRepository.findOne(id);
    return drawing;
  }


  // public async returnBook(
  //   bookId: number,
  //   userId: number,
  // ): Promise<BorrowedBookLog> {
  //   const bookToBeReturned = await this.bookRepository.findOne({
  //     where: {
  //       id: bookId,
  //     },
  //     relations: ['borrowedBooks'],
  //   });
  //   console.log(bookToBeReturned);
  //   if (!bookToBeReturned) {
  //     throw new NotFoundException(`Not a valid book to return!!!`);
  //   }

  //   bookToBeReturned.isBorrowed = false;

  //   const user = await this.userRepository.findOne(userId);

  //   await this.bookRepository.save(bookToBeReturned);

  //   const borrowBookLog = await this.borrowBookRepository.findOne({
  //     where: {
  //       book: bookId,
  //       user: userId,
  //     },
  //   });
  //   borrowBookLog.hasReturned = true;

  //   await this.borrowBookRepository.save(borrowBookLog);

  //   // TODO: Give reading points also when the user writes a review or when his review gets liked.
  //   // TODO: Subtract reading points when the user gets banned
  //   // TODO: Add the reading points to the UserDto
  //   user.readingPoints += BooksService.READING_POINTS_FOR_RETURNED_BOOK;
  //   await this.userRepository.save(user);

  //   return borrowBookLog;
  // }

 
}
