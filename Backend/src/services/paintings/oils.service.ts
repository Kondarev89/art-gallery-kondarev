import { User } from '../../entity/user.entity';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Watercolours } from 'src/entity/watercolour.entity';
import { Oils } from 'src/entity/oils.entity';
import { ReturnPaintingDTO } from 'src/dtos/return.painting.dto';

@Injectable()
export class OilsService {

  constructor(

    @InjectRepository(Oils) private readonly oilRepository: Repository<Oils>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,

  ) {}


  async getAllOils(): Promise<ReturnPaintingDTO[]> {
    const oils = await this.oilRepository.find();
    return oils;
  }


  async getOilsById(id: number): Promise<Oils> {
    const oil= await this.oilRepository.findOne(id);
    return oil;
  }

  async createOil(title: string): Promise<Watercolours> {
    const checkOil = await this.oilRepository.findOne({
      where: {
        title: title,
      },
    });
    if (checkOil) {
      throw new BadRequestException('This painting already exist!');
    }
    const newOil = new Oils();
    newOil.title = title;
    return this.oilRepository.save(newOil);
  }
  async deleteWatercolour(paintingId: number) {
    const watercolour = await this.oilRepository.delete(paintingId);
    return watercolour;
  }
}
