import { User } from '../../entity/user.entity';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Watercolours } from 'src/entity/watercolour.entity';
import { ReturnPaintingDTO } from 'src/dtos/return.painting.dto';

@Injectable()
export class WatercolourService {

  constructor(
    @InjectRepository(Watercolours) private readonly watercolourRepository: Repository<Watercolours>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,

  ) {}
  async getAllWatercolours(): Promise<ReturnPaintingDTO[]> {
    const watercolour = await this.watercolourRepository.find();
    return watercolour;
  }
  
  async getWatercoloursById(id: number): Promise<Watercolours> {
    const painting = await this.watercolourRepository.findOne(id);
    return painting;
  }

  // public async returnBook(
  //   bookId: number,
  //   userId: number,
  // ): Promise<BorrowedBookLog> {
  //   const bookToBeReturned = await this.bookRepository.findOne({
  //     where: {
  //       id: bookId,
  //     },
  //     relations: ['borrowedBooks'],
  //   });
  //   console.log(bookToBeReturned);
  //   if (!bookToBeReturned) {
  //     throw new NotFoundException(`Not a valid book to return!!!`);
  //   }

  //   bookToBeReturned.isBorrowed = false;

  //   const user = await this.userRepository.findOne(userId);

  //   await this.bookRepository.save(bookToBeReturned);

  //   const borrowBookLog = await this.borrowBookRepository.findOne({
  //     where: {
  //       book: bookId,
  //       user: userId,
  //     },
  //   });
  //   borrowBookLog.hasReturned = true;

  //   await this.borrowBookRepository.save(borrowBookLog);

  //   // TODO: Give reading points also when the user writes a review or when his review gets liked.
  //   // TODO: Subtract reading points when the user gets banned
  //   // TODO: Add the reading points to the UserDto
  //   user.readingPoints += BooksService.READING_POINTS_FOR_RETURNED_BOOK;
  //   await this.userRepository.save(user);

  //   return borrowBookLog;
  // }

  async createWatercolour(title: string): Promise<Watercolours> {
    const checkWatercolour = await this.watercolourRepository.findOne({
      where: {
        title: title,
      },
    });
    if (checkWatercolour) {
      throw new BadRequestException('This painting already exist!');
    }
    const newPainting = new Watercolours();
    newPainting.title = title;
    return this.watercolourRepository.save(newPainting);
  }
  async deleteWatercolour(paintingId: number) {
    const watercolour = await this.watercolourRepository.delete(paintingId);
    return watercolour;
  }
}
