import { AuthService } from './users/auth.service.';
import { JwtStrategy } from './strategy/jwt.strategy';
import { jwtConstants } from './../constant/secret';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UsersService } from './users/users.services';
import { User } from '../entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Token } from 'src/entity/token.entity';
import { WatercolourService } from './paintings/watercolours.service';
import { OilsService } from './paintings/oils.service';
import { Drawings } from 'src/entity/drawings.entity';
import { Oils } from 'src/entity/oils.entity';
import { Watercolours } from 'src/entity/watercolour.entity';
import { TransformServices } from './transform.services';
import { DrawingsService } from './paintings/drawings.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Token,
      Drawings,
      Oils,
      Watercolours,

    ]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      },
    }),
  ],
  controllers: [],
  providers: [
    WatercolourService,
    OilsService,
    DrawingsService,
    UsersService,
    JwtStrategy,
    AuthService,
    TransformServices,
  ],
  exports: [
    WatercolourService,
    OilsService,
    DrawingsService,
    UsersService,
    AuthService,
    TransformServices,
  ],
})
export class ServicesModule {}
