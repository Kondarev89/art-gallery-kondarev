import { UserRole } from './../enum/user.role';
import { Injectable } from '@nestjs/common';
import { User } from 'src/entity/user.entity';
import { UserDTO } from 'src/dtos/user.dto';

@Injectable()
export class TransformServices {
 
  transformUser(user: User): UserDTO {
    return {
      id: user.id,
      username: user.username,
      role: UserRole[user.role],
      isDeleted: user.isDeleted,
      
    };
  }
}
