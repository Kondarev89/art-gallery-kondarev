import { JWTPayload } from '../../common/jwt-payload';
import { jwtConstants } from '../../constant/secret';
import { AuthService } from '../users/auth.service.';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }
  async validate(payload: JWTPayload) {
    return await this.authService.findUserByName(payload.username);
  }
}
