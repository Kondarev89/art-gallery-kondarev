import { ServicesModule } from 'src/services/service.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ControllersModule } from './controllers/controllers.module';

@Module({
  imports: [
    ControllersModule,
    ServicesModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '11235813',
      database: 'artgallerydb',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
  ],
  providers: [],
})
export class CoreModule {}
