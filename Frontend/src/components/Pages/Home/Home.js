import React from "react";
import "./Home.css";
import HomeCarousel from "../Carousel/Carousel";
import Authentic from "./Authentic";
import Rights from "../../Rights/Rights";
import Commercial from "./Commercial";


const home = () => {
  return (
    <div>
      <div>
        <HomeCarousel />
        <Authentic />
        <Commercial />
        {/* <Movie url = "https://www.youtube.com/watch?v=PlMJyazlnUg" /> */}
        <Rights />
      </div>
    </div>

  )
};

export default home;