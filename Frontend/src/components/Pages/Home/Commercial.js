
import React from "react";
import Button from "react-bootstrap/Button";
import chirpan from "../../../images/chirpan.JPG"
import Jumbotron from 'react-bootstrap/Jumbotron'
import "./Commercial.css"


const Commercial = () => {
    return (
        <div class="img-wrapper">
            <img class="img-responsive" alt=''
                src={chirpan} />
            <div class="img-overlay">

                <div id="example-collapse-text">
                    <Jumbotron id="jt" fluid={true} bsPrefix="jumbotron">
                       

                            <p id="deba" >Vladimir Kondarev</p>
                            <p id="deba2" style={{ color: "white",  fontSizeAdjust: "auto", fontWeight: 'bold', fontFamily: "Cambria", width: "auto", height: "auto", display: "block", }}>"The Big Art In The Small Town"</p>
                       
                    </Jumbotron>

                    <Button  block={false}
                        // onMouseEnter={() => setShowButton(true)}
                        // onMouseLeave={() => setShowButton(false)}

                        variant="light" size="sm" href="https://www.youtube.com/watch?v=CmcvbREp5pI" id="cmBtn"
                        style={{ color: "white", fontSize: 24, fontWeight: 'bold', fontFamily: "Cambria", width: "auto" }} >  Movie At YouTube </Button>

                </div>
            </div>
        </div>
    )
}

export default Commercial;

