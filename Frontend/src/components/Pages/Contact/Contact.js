import React, { useState } from 'react';
import Rights from '../../Rights/Rights';
import Swal from 'sweetalert2'
import "./Contact.css"
import emailjs from "emailjs-com";
import Wrapper from '../../UI/Wrapper';
import ErrorModal from "../../UI/ErrorModal";
import PopUpModal from '../../UI/PopUpModal';




const GalleryContactForm = (props) => {
  const [pop, setPop] = useState();
  const [enteredName, setEnteredName] = useState('');
  const [enteredEmail, setEnteredEmail] = useState('');
  const [enteredSubject, setEnteredSubject] = useState('');
  const [enteredMessage, setEnteredMessage] = useState('');
  const [error, setError] = useState();

  const nameChangeHandler = (event) => {
    setEnteredName(event.target.value);
  }
  const emailChangeHandler = (event) => {
    setEnteredEmail(event.target.value);
  }
  const subjectChangeHandler = (event) => {
    setEnteredSubject(event.target.value);
  }

  const messageChangeHandler = (event) => {
    setEnteredMessage(event.target.value);
  }

  let sendEmail = (e) => {
    e.preventDefault();
    if (enteredName.trim().length === 0 || enteredEmail.trim().length === 0 ||enteredSubject.trim().length === 0 || enteredMessage.trim().length === 0 || enteredMessage.trim().length === 0) {
      setError({
        title: "Invalid input!",
        message: "Please enter a valid Name, E-Mail, Subject and a Message!"
      })
      return;
    };

    console.log(enteredName, enteredEmail)
    setEnteredName('');
    setEnteredEmail('');
    setEnteredSubject('');
    setEnteredMessage('');


    emailjs.sendForm('service_j3rhdy9', 'template_hyqw4qv', e.target, 'user_0p2XfF9mvEiKgscD1mv2q')
       .then((result) => {
        //  alert("Message SEND!", result.text);
        Swal.fire("Thank You for Your Message!", result.text)
      }, (error) => {
        console.log(error.text);
      });
    e.target.reset("Message send!")

  };

  const errorHandler = () => {
    setError(null);
  }

  return (
    <Wrapper>
      {error && <ErrorModal title={error.title} message={error.message} onConfirm={errorHandler} />}
      <div id="contact">
        <div className="container" >
          <p style={{ color: "black", fontSize: 60, fontWeight: 'bold', fontFamily: "Cambria", }}> E-mail Form: </p>
          <p style={{ color: "black", fontSize: 38, fontWeight: 'bold', fontFamily: "Cambria", }}>We are waiting on your questions and desires!</p>
          <form onSubmit={sendEmail}>
            <div className="row pt-5 mx-auto">

              <div className="col-8 form-group mx-auto" >
                <input type="text" className="form-control" placeholder="Name" name="name" value={enteredName} onChange={nameChangeHandler} />
              </div>
              <div className="col-8 form-group pt-2 mx-auto">
                <input type="email" className="form-control" placeholder="E-mail Address" name="email" value={enteredEmail} onChange={emailChangeHandler} />
              </div>
              <div className="col-8 form-group pt-2 mx-auto">
                <input type="text" className="form-control" placeholder="Subject" name="subject" value={enteredSubject} onChange={subjectChangeHandler} />
              </div>
              <div className="col-8 form-group pt-2 mx-auto">
                <textarea className="form-control" id="" cols="30" rows="10" placeholder="Your message goes in here!" name="message" value={enteredMessage} onChange={messageChangeHandler}></textarea>
              </div>
              <div className="col-8 form-group pt-2 mx-auto">
                <input type="submit" className="btn btn-info" value="Send Message" id="fmbtn"
                  style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Cambria", }}
                  onClick={setPop}></input>
              </div>
            </div>
          </form>
        </div>
      </div>
      <Rights />
    </Wrapper>
  )





};

export default GalleryContactForm;


