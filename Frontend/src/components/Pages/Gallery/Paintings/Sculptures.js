
import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../../../providers/UserContext"
import { BASE_URL } from "../../../../common/constants"
import Button from "react-bootstrap/Button"
import UnderConstruction from "../../../Under Construction/underConstruction";


const Sculptures = (props) => {
    const [sculptures, setSculptures] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/sculptures`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
            },
        })
            .then((r) => r.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }

                setSculptures(data);
            })
            .catch((error) => setError(error.message))
            .finally(() => setLoading(false));
    }, []);

    return (
        <div class="wrapper">
         <UnderConstruction />
        </div>

    )
};

export default Sculptures;