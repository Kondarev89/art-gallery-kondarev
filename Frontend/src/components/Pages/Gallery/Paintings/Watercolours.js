
import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../../../providers/UserContext"
import { BASE_URL } from "../../../../common/constants"
import Button from "react-bootstrap/Button"
import UnderConstruction from "../../../Under Construction/underConstruction";


const Watercolours = (props) => {
    const [watercolours, setWatercolours] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/aquarels`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
            },
        })
            .then((r) => r.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }

                setWatercolours(data);
            })
            .catch((error) => setError(error.message))
            .finally(() => setLoading(false));
    }, []);

    const displayWatercolours = () => {
        return watercolours.map(wc => {
            return <div urlPoster={wc.urlPoster}/>
        })
    }
    return (
        <div class="wrapper">
            <section>{displayWatercolours}</section>
        </div>
    )
};

export default Watercolours;