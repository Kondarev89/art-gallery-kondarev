
import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../../../providers/UserContext"
import { BASE_URL } from "../../../../common/constants";
import "./Drawings.css"


const Drawings = (props) => {
    const [drawings, setDrawings] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/drawings`, {
            // headers: {
            //     "Content-Type": "application/json",
            //     Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
            // },
        })
            .then((data) => data.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }

                setDrawings(data);
            })
            .catch((error) => setError(error.message))
            .finally(() => setLoading(false));

    }, []);


    console.log(drawings)
    return (
        <div class="container">
            <div id="drw">
                <h1 id="sp">
                    {drawings.map((d) => <img width={"200px"} height={"200px"} src={d.urlPoster} title={d.title} key={d.id} style={{ paddingLeft: "20px", paddingBottom: "20px", }}></img>)}
                </h1>
            </div>

        </div>
    )
};


export default Drawings;



// {drawings.map((d) => <img width={"200px"} height={"200px"} src={d}></img>)}
