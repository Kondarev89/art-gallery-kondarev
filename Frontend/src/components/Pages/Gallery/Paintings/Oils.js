
import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../../../providers/UserContext";
import { BASE_URL } from "../../../../common/constants"
import UnderConstruction from "../../../Under Construction/underConstruction"


const Oils = (props) => {
    const [oils, setOils] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/oils`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
            },
        })
            .then((r) => r.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }

                setOils(data);
            })
            .catch((error) => setError(error.message))
            .finally(() => setLoading(false));
    }, []);

    return (
        <div class="wrapper">
            <UnderConstruction />
        </div>
    )
};

export default Oils;