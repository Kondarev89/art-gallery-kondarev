import React from 'react';
import UC from "../../../images/UC.jpg"
import "./Gallery.css"
import Rights from "../../../components/Rights/Rights"
import underConstruction from "../../Under Construction/underConstruction"
import { Link } from 'react-router-dom';

const Gallery = () => {

  return (<>
    <div id="Gallery">
      <div class="container" id="container">
        <div class="row">
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <h4 class="card-title1" style={{ color: "black", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> Oils</h4>
            <div class="card" id="GalleryOils">
              <Link to="/oils"><div class="card-img-overlay text-white d-flex flex-column justify-content-center">
                <h4 class="card-title1" style={{ color: "black", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> </h4>
              </div>
                <img class="card-img" src="https://i.postimg.cc/DynQRZM8/Oils1.jpg" ></img>
              </Link>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <h4 class="card-title2" title="buzz hover text" style={{ color: "black", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> Watercolours</h4>
            <div class="card" id="GalleryWatercolours">
              <Link to="/watercolours"><img class="card-img" src="https://i.postimg.cc/FF85xMWW/WC1.jpg" ></img>
                <div class="card-img-overlay text-white d-flex flex-column justify-content-center">
                </div>
              </Link>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <h4 class="card-title3" style={{ color: "black ", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> Drawings </h4>
            <div class="card" id="GalleryDrawings">
              <Link to="/drawings"><img class="card-img" src="https://i.postimg.cc/MGM11jvr/DW1.jpg"></img>
                <div class="card-img-overlay text-white d-flex flex-column justify-content-center">
                </div>
              </Link>
            </div>
          </div>
          {/* <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <h4 class="card-title1" style={{ color: "black", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> Mixed Media</h4>
            <div class="card" id="GalleryOils">
              <Link to="/mixTechnique"><div class="card-img-overlay text-white d-flex flex-column justify-content-center">
                <h4 class="card-title1" style={{ color: "black", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> </h4>
              </div>
                <img class="card-img" src="https://i.postimg.cc/LXjX1jkV/2202254.jpg" ></img>
              </Link>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <h4 class="card-title2" title="buzz hover text" style={{ color: "black", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> Sculptures </h4>
            <div class="card" id="GalleryWatercolours">
              <Link to="/sculptures"><img class="card-img" src="https://i.postimg.cc/LXjX1jkV/2202254.jpg" ></img>
                <div class="card-img-overlay text-white d-flex flex-column justify-content-center">
                </div>
              </Link>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <h4 class="card-title3" style={{ color: "black ", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}> Illustrations </h4>
            <div class="card" id="GalleryDrawings">
              <Link to="/bookillustrations"><img class="card-img" src="https://i.postimg.cc/LXjX1jkV/2202254.jpg"></img>
                <div class="card-img-overlay text-white d-flex flex-column justify-content-center">
                </div>
              </Link>
            </div>
          </div> */}
        </div>
      </div>
    </div>
    <Rights />
  </>);
};

export default Gallery;





{/* <div class="myDIV">Hover over me.</div>
<div class="hide">I am shown when someone hovers over the div above.</div> */}


// <div id="Gallery" >
//   <div id="Gallery" style={{ display: 'flex', flexDirection: 'row', background: "transparent", paddingLeft: "60px" }} >
//     <div>

//       <div class="myDIV"> <li><a href="/Gallery/Oils"><img src="https://i.postimg.cc/fyvcpH2k/DSC-1356.jpg" id="GalleryOils" style={{ height: "auto", width: "75%", display: 'block', flex: 1 }} /></a></li></div>
//       <div class="hide" style={{ color: "black", fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }}> Oils </div>
//     </div>
//     <div>
//       <div class="myDIV">  <li><a href="/Gallery/Watercolours"><img src="https://i.postimg.cc/7PCPsYtm/625497-161916467306963-1887250104-n.jpg" id="GalleryWatercolours" style={{ height: "auto", width: "75%", flex: 1 }} /></a></li></div>
//       <div class="hide" style={{ color: "black", fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }}> Watercolours </div>
//     </div>
//     <div>
//       <div class="myDIV">   <li><a href="/Gallery/Drawings"><img src="https://i.postimg.cc/T2WGjNhs/90356931-1668175933347668-8555835201664581632-o.jpg" id="GalleryDrawings" style={{ height: "auto", width: "75%", flex: 1 }} /></a></li></div>
//       <div class="hide" style={{ color: "black", fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }}> Drawings </div>
//     </div>
//     <div>
//       <div class="myDIV">  <li><a href="/Gallery/Sculptures"><img src="https://i.postimg.cc/SRJQ6Z0S/kone.jpg" id="GallerySculptures" style={{ height: "auto", width: "75%", flex: 1 }} /></a></li></div>
//       <div class="hide" style={{ color: "black", fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }}> Sculptures </div>
//     </div>
//   </div>
// </div> 

