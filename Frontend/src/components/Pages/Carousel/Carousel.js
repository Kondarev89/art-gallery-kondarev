import Carousel from 'react-bootstrap/Carousel';
import React from "react";
import MainOil from "../../../images/MainOil.jpg"
import MainWatercolour from "../../../images/MainWatercolour.jpg"
import MainDrawings from "../../../images/MainDrawings.jpg"
import Button from "react-bootstrap/Button"
import "./Carousel.css"


const HomeCarousel = (history) => {
  return (
    <selection>
      <Carousel id="carousel" touch={true} slide={true} controls={true} indicators={true} expanded={true} pause={false} wrap={true} fade={false}>
        <Carousel.Item interval={3000} >
          <img
            class="img-fluid"
            src={MainOil}
            style={{ height: "auto", maxHeight:950,width: '100%', overflow: 'hidden' }}
            alt="First slide"
            controls="true"
            id="carousel1"
          />
          <Carousel.Caption className="first">
           
            <p id="carr" style={{ color: "black", fontWeight: 'bold', fontFamily: "Cambria", margin: 0}}> Quality Oil Paintings</p>
            <p id="carr2"  style={{ color: "black",  fontWeight: 'bold', fontFamily: "Cambria" }}> Authentic Artworks! </p>
           
            <Button variant="light" size="lg" href="/gallery" id="btn" bsPrefix="btn" style={{ color: "Black", fontSize: 24, fontWeight: 'bold', fontFamily: "Cambria" }} > Oil Paintings </Button>
            
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={3000} >
          <img
            
            class="img-fluid"
            src={MainWatercolour}
            style={{ height: "auto", maxHeight:950,  width: '100%', overflow: 'hidden' }}
            alt="Second slide"
            controls="true"
            id="carousel2"
          />
          <Carousel.Caption>
            <p id="carr" style={{ color: "black",  fontWeight: 'bold', fontFamily: "Cambria", margin: 0 }}> Vibrant Watercolours </p>
            <p id="carr2" style={{ color: "black", fontWeight: 'bold', fontFamily: "Cambria" }}> Newest In Our Collection!</p>
            <Button variant="light" href="/gallery" size="lg" id="btn" style={{ color: "Black", fontSize: 24, fontWeight: 'bold', fontFamily: "Cambria" }}> Watercolours </Button>

          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={4000}>
          <img
            class="img-fluid"
            src={MainDrawings}
            style={{ height: "auto", maxHeight:950, width: '100%', overflow: 'hidden' }}
            alt="Third slide"
            controls="true"
            id="carousel3"
          />
          <Carousel.Caption id="thirth" >
            {/* <h3 style={{ color: "rgb(17, 17, 17)", fontSize: 30, fontWeight: 'bold', fontFamily: "Cambria" }}> Passionate Drawings </h3>
            <p style={{ color: "rgb(17, 17, 17)", fontSize: 30, fontWeight: 'bold', fontFamily:"Cambria"}}>  Expessions of the Artist Soul </p> */}

            <Button variant="dark" href="/gallery" size="lg" id="btn" style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Cambria" }}> Drawings </Button>
            
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </selection>
  )
};

export default HomeCarousel;