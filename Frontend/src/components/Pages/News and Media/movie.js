import React from "react";
import ReactPlayer from "react-player"
import "./Movie.css"



const Movie = ({ url }) => {
  
  return (
    <div >
    <div id="pbg" className="card rounded border-0 " >

      <div className="player-wrapper" id="pbg" >
        <ReactPlayer
          id="player"
          className="react-player"
          url={url}
          width="100%"
          height="100%"
          controls={true}
        />
      </div>

    </div>
    </div>

  )

}


export default Movie;




// <div className="video-responsive">
// <video 
//  autoPlay 
//  loop
//   muted 
//   style={{ 
//     position: "absolute", 
//     width: "100%", 
//     left: "50%", 
//     top: "50%", 
//     height: "100%", 
//     objectFit: "cover",
//     transform: "translate(-50%, -50%)",
//     zIndex: "-1",
//     }}>
//   <source src={KondarevTheMovie} type="video/mp4" />
// </video>
// </div>