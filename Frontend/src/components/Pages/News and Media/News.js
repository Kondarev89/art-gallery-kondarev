import React from 'react';
import vk5 from "../../../images/vk5.jpg"
import "./News.css"


const News = () => {


  return (<>
    <section className="section" id="backgroundNews">
      <div className="container" id="nwz" >
        <a id="featureText" href="https://www.facebook.com/vladimir.kondarev.3"  >
          <p id="news" className="title" style={{ color: "black",  fontWeight: 'bold', fontFamily: "Cambria"}}>Vladimir Kondarev</p></a>
        <p id="news2" style={{ color: "black",  fontWeight: 'bold', fontFamily: "Cambria", textAlign: "center" }}>
          Latest interview with the Artist about his new exibition in the city of Stara Zagora on May the 7th 2021 year.
        </p>
        <div id="kont">
          <div className="col-xl-6">
            <a href="https://bnr.bg/starazagora/post/101449893/hudojnikat-vladimir-kondarev-risuva-na-edin-dah">  < img src={vk5} alt="" id="kon11" style={{ height: "auto", width: '100%', overflow: 'hidden', borderRadius: "2%", marginTop: "50px", display: "block" }} /> </a>
          </div>
          <div className="col-lg-6">
          </div>
        </div>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
      </div>
    </section>
  </>);
};

export default News;
