import React from "react";
import pv from "../../../images/pv.jpg";
import WA from "../../../images/WA.jpg";
import Rights from "../../Rights/Rights";
import "./Media.css";
import News from "./News";
import img10 from "../../../images/img10.jpg";
import Movie from "./Movie";


const NewsAndMedia = () => {


    return (<>
        <News />
        <Movie url = "https://www.youtube.com/watch?v=CmcvbREp5pI"/>
        <div id="bgrnd">
            <p id="para1"><h3 style={{ color: "black", fontSize: 28, fontWeight: 'bold', fontFamily: "Cambria" }}>Articles about Vladimir Kondarev :</h3></p>
            <div id="NewsAndMediaCont" style={{ display: 'flex', flexDirection: 'row', background: "transparent", }} >
                <li id="li" key=""><a href="https://www.duma.bg/plasticheskite-vazdishki-na-vladimir-kondarev-n188621?h=pdfarchive&pdfarchiveId=2974"><img src={pv} id="cardImg2" style={{ height: "auto", width: "75%", display: 'block', flex: 1 }} /></a></li>
                <li id="li" key="" ><a href="https://duma.bg/yubileyna-izlozhba-na-vladimir-kondarev-n12919"><img src={WA} id="cardImg3" style={{ height: "auto", width: "75%", display: 'block', flex: 1 }} /></a></li>
                <li id="li" key="" ><a href="https://dolap.bg/2019/07/16/%D0%BF%D0%BE%D1%80%D1%82%D0%BE%D0%BA%D0%B0%D0%BB%D0%BE%D0%B2%D0%B8%D1%82%D0%B5-%D0%B6%D0%B5%D0%BD%D0%B8-%D0%BD%D0%B0-%D0%B2%D0%BB%D0%B0%D0%B4%D0%BE-%D0%BA%D0%BE%D0%BD%D0%B4%D0%B0%D1%80%D0%B5%D0%B2/"><img src={img10} id="cardImg2" style={{ height: "auto", width: "63%", display: 'block', flex: 1 }} /></a></li>
                <li id="li" key="" ><a href="https://dolap.bg/2021/04/09/%D0%B8%D0%B7%D0%BB%D0%BE%D0%B6%D0%B1%D0%B0-%D0%BD%D0%B0-%D0%B5%D0%B4%D0%B8%D0%BD-%D0%B4%D1%8A%D1%85-%D0%BF%D1%80%D0%B5%D0%B4%D1%81%D1%82%D0%B0%D0%B2%D0%B8-%D0%B2%D0%BB%D0%B0%D0%B4/"><img src={img10} id="cardImg3" style={{ height: "auto", width: "63%", display: 'block', flex: 1 }} /></a></li>
                <li id="li" key="" ><a href="https://dolap.bg/2021/04/09/%D0%B8%D0%B7%D0%BB%D0%BE%D0%B6%D0%B1%D0%B0-%D0%BD%D0%B0-%D0%B5%D0%B4%D0%B8%D0%BD-%D0%B4%D1%8A%D1%85-%D0%BF%D1%80%D0%B5%D0%B4%D1%81%D1%82%D0%B0%D0%B2%D0%B8-%D0%B2%D0%BB%D0%B0%D0%B4/"><img src={img10} id="cardImg3" style={{ height: "auto", width: "63%", display: 'block', flex: 1 }} /></a></li>

            </div>
        </div>

        <Rights />

    </>);
};

export default NewsAndMedia;
