import React from "react";
import "./Biography.css";
import v2 from "../../../images/v2.jpg"
import Rights from "../../Rights/Rights";
import kondarev from "../../../images/kondarev.jpg"
const Bio = () => {
    return (<>
        <section className="section" id="backgroundBio">
            <div className="container" id="backgroundBio">
            <a href="https://www.facebook.com/vladimir.kondarev.3" style={{ color: "black", fontSize: 60, fontWeight: 'bold', fontFamily: "Cambria", }}><h1 className="title" style={{ color: "black", fontSize: 60, fontWeight: 'bold', fontFamily: "Cambria", }}>Vladimir Kondarev</h1></a>
                <p style={{ color: "black", fontSize: 32, fontWeight: 'bold', fontFamily: "Cambria", textAlign: "center" }}>
            Renown Bulgarian Artist  <br></br> Currator of Municipal Art Gallery "Georgi Danchov - Zograff" in the city of Chirpan.
            </p>
                <div className="card rounded border-0 " id="backgroundBio">


                    <div className="row no-gutters  align-items-center" id="kont">
                        <div className="col-xl-6">
                            <img src={kondarev} alt="" id="kon1" style={{ height: "auto", width: '100%', overflow: 'hidden' }} />
                            <br></br>
                            <br></br>
                            <br></br>
                            <br></br>
                            <img src={v2} alt="" id="kon2" style={{ height: "auto", width: '100%', overflow: 'hidden' }} />
                        </div>
                        <div className="col-lg-6">
                            <div className="card-body section-title p-md-5" id="t">
                                {/* <a href="https://www.facebook.com/vladimir.kondarev.3" style={{ color: "black", fontSize: 60, fontWeight: 'bold', fontFamily: "Cambria" }}><h4 className="title">Vladimir Kondarev</h4></a> */}
                                <p className="biotext">Vladimir Kondarev is born in the small city of Chirpan in the year 1959. Passionate about Art and Sculpture,
                                in 1974 He is accepted to study in The National High School of Plastic Arts and Design “Academician Dechko Uzunov”
                                under the guidance of the famous scultor Leonid Micev. After graduation Kondarev does a number of specialisations in Monumental Sculpture in Sofia from 1982 till 1986
                                                under the guidance of Prof. Dimitar Boykov and Maya Georgieva!<br>
                                    </br> At Sofia his love and passion about Drawings and Watercolours grew and in time those
                                                two styles became his trademark and some of his greatest Art Works. Kondarev has a lot of submissions in galleries
                                                and exibitions in Bulgaria and around the World. He is an author of Many Solo Exibitions in the biggest Bulgarian cities, some of
                                                which are as mentioned below : <br>

                                    </br>
                                                1. 1989 - "Naked Body" - Dawings <br>
                                    </br>
                                                2. 1994 -  "Watercolours and Drawings"   - Solo exibition in the city of Pomorie <br>
                                    </br>
                                                3. 2005 - "Watercolours, Drawings and Mix Techniques" <br>
                                    </br>
                                                4. 2006 - "The Way to Home" - Solo exibition in multiple cities<br>
                                    </br>
                                                5. 2007 - "Levski and the Time" - exibition in the ciry of Pazardzhik<br>
                                    </br>
                                                6. 2008 - Exibition in Bulgarian National Radio - Stara Zagora <br>
                                    </br>
                                                7. 2009 - "Balabanov's house" - exibition in city of Plovdiv<br>
                                    </br>
                                                8. 2010 - "Reflections" -  Solo exibition in the city of Karlovo <br>
                                    </br>
                                                9. 2015 - Solo Exibition Under the Influence of the Poets : Pavel Matev, Dimitur Danailov and Angel Krosnev. <br>
                                    </br>  <br></br>
                                </p>
                            </div>
                        </div>
                        <p id="bot" style={{ color: "black", fontSize: 26, fontFamily: "Cambria" }}>
                    Kondarev's  Art Works are sold and in possession of private collectors , banks and businesses in Bulgaria and all over the World. 
                    Since 1987 Kondarev settles down in his home city of Chirpan and He is the Currator of the Municipal Art Gallery "Georgi Danchov - Zograff" .
                </p>
                    </div>
                </div>
            </div>
        </section>
        <Rights />
    </>
    )

};

export default Bio;
