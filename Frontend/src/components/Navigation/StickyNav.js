import React, { useState, useEffect } from "react";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from 'react-bootstrap/NavDropdown'
import "./Navigation.css"
import SignIn from "../SignIn/SignIn"
import Contact from "../../components/Pages/Contact/Contact"


const Sticky = () => {
  const [barVisibility, setBarVisibility] = useState(false);
  const [scrollPosition, setSrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    position >= 120 ? setBarVisibility(true) : setBarVisibility(false);
    setSrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div id="topBar" className={barVisibility ? "showBar" : "hideBar"}>
    <nav className="navbar fixed-top">
      <Navbar   expand="lg" variant="dark" bg="green" id="navbar">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" id="collapse"/>
        <Navbar.Collapse id="responsive-navbar-nav" className="collapse">
          <Navbar.Brand href="/home" id="vk" className="link" >Vladimir Kondarev</Navbar.Brand>
          <Nav className="mr-auto"  >
            <Nav.Link href="/home" id="home" >Home</Nav.Link>
            <Nav.Link href="/about" id="about" >About</Nav.Link>
            <Nav.Link href="/bio" id="bio">Biography</Nav.Link>
            <Nav.Link href="/contact" id="contact" >Contact</Nav.Link>
            <Nav.Link href="/news" id="news" >News</Nav.Link>
            <Nav.Link href="/gallery" id="gallery" >Gallery</Nav.Link>
            <Nav.Link href="/news" id="faq" >FAQ</Nav.Link>

          </Nav>
          </Navbar.Collapse>
        </Navbar>
    </nav>
  </div>
  );
};

export default Sticky;
