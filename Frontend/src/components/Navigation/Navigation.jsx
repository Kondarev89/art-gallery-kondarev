import React, { useContext, useState, useEffect } from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import "./Navigation.css"
import Button from "react-bootstrap/Button"
import userContext from "../../providers/UserContext"
import { withRouter } from 'react-router-dom';


const Navigation = (props) => {
  const [barVisibility, setBarVisibility] = useState(false);
  const [scrollPosition, setSrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    position >= 120 ? setBarVisibility(true) : setBarVisibility(false);
    setSrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div id="topBar" className={barVisibility ? "showBar" : "hideBar"}>
      <nav className="navbar fixed-top " id="nav1">
        <Navbar expand="xl" variant="dark" bg="green" id="collapse" >
          <Navbar.Toggle aria-controls="responsive-navbar-nav" id="collapse" />
          <Navbar.Collapse id="responsive-navbar-nav" className="collapse" >
            <Navbar.Brand href="/home" id="vk" className="link" id="vk" ><img src="https://i.postimg.cc/02cjY8js/avtograff.png" style={{ height: "auto", width: "60%" }} /></Navbar.Brand>
            <Nav class="navbar-nav justify-content-between w-100"  >
              <Nav.Link href="/home" className="home" style={{ color: "rgba(255,255,255,.5", fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }} >Home</Nav.Link>
              <Nav.Link href="/bio" className="bio" style={{ color: "rgba(255,255,255,.5", fontWeight: 'bold', fontSize: 34,fontFamily: "Cambria" }}>Biography</Nav.Link>
              <Nav.Link href="/gallery" className="gallery" style={{ color: "rgba(255,255,255,.5",fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }}>Gallery</Nav.Link>
              <Nav.Link href="/newsandmedia" className="news" style={{ color: "rgba(255,255,255,.5", fontSize: 34,fontWeight: 'bold', fontFamily: "Cambria" }}>News and Media</Nav.Link>
              <Nav.Link href="/contact" className="contact" style={{ color: "rgba(255,255,255,.5", fontSize: 34,fontWeight: 'bold', fontFamily: "Cambria" }} >Contact</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </nav>
    </div>
  );
};

export default withRouter(Navigation);
// navbar-expand-lg
// class="navbar-nav justify-content-between w-100"








// <div id="topBar" className={barVisibility ? "showBar" : "hideBar"} expand="true" collapseOnSelect="true">
//   <Nav className="navbar fixed-top navbar-left" id="collapse">
//     <Navbar.Brand href="/home" id="vk" className="link" id="vk" style={{ color: "rgba(255,255,255,.5", fontSize: 34, fontWeight: 'bold', fontFamily: "Cambria" }}><img src="https://i.postimg.cc/02cjY8js/avtograff.png" style={{ height: "auto", width: "65%" }} /></Navbar.Brand>
//     <Nav.Link href="/home" className="home" style={{ color: "rgba(255,255,255,.5", fontWeight: 'bold', fontFamily: "Cambria" }}>Home</Nav.Link>
//     <Nav.Link href="/bio" className="bio" style={{ color: "rgba(255,255,255,.5", fontWeight: 'bold', fontFamily: "Cambria" }}> Biography </Nav.Link>
//     <Nav.Link href="/gallery" className="gallery" style={{ color: "rgba(255,255,255,.5", fontWeight: 'bold', fontFamily: "Cambria" }}> Gallery </Nav.Link>
//     <Nav.Link href="/newsandmedia" className="news" style={{ color: "rgba(255,255,255,.5", fontWeight: 'bold', fontFamily: "Cambria" }}> News and Media </Nav.Link>
//     <Nav.Link href="/contact" className="contact" style={{ color: "rgba(255,255,255,.5", fontWeight: 'bold', fontFamily: "Cambria" }}> Contact </Nav.Link>

//   </Nav>

// </div>

