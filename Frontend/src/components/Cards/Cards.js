import React from 'react'
import CardGroup from 'react-bootstrap/CardGroup'
import Card from 'react-bootstrap/Card'
import "./Cards.css"
import v3 from "../../images/v3.jpg"

const CardExampleCard = () => {
  return (
    <div>
      <CardGroup id="group">
  <Card id="card1">
    <Card.Img variant="top" src={v3} id="cardImg"  style={{height: "360px", width: "80%", display: 'block'}}/>
    <Card.Body id="body">
      <Card.Title id="title">Card title</Card.Title>
      <Card.Text id="text">
        This is a wider card with supporting text below as a natural lead-in to
        additional content. This content is a little bit longer.
      </Card.Text>
    </Card.Body>
  </Card>
  <Card>
    <Card.Img variant="top" src={v3} id="cardImg" style={{height: "360px", width: "80%", display: 'block'}}/>
    <Card.Body>
      <Card.Title>Card title</Card.Title>
      <Card.Text >
        This card has supporting text below as a natural lead-in to additional
        content.{' '}
      </Card.Text>
    </Card.Body>
  </Card>
  <Card>
    <Card.Img variant="top" src={v3} id="cardImg" style={{height: "360px", width: "80%", display: 'block'}}/>
    <Card.Body>
      <Card.Title>Card title</Card.Title>
      <Card.Text >
        This is a wider card with supporting text below as a natural lead-in to
        additional content. This card has even longer content than the first to
        show that equal height action.
      </Card.Text>
    </Card.Body>
  </Card>
</CardGroup>
    </div>
  )
};

export default CardExampleCard;
