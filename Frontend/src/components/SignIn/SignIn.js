import React, { useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import {BASE_URL} from '../../common/constants'
import UserContext from '../../providers/UserContext';
import jwtDecode from 'jwt-decode';
import "./SignIn.css";


const SignIn = (props) => {
  const history = props.history;
  const location = props.location;


  const { setUser } = useContext(UserContext);



  const [user, setUserObject] = useState({
    username: {
      value: '',
      touched: false,
      valid: true,
    },
    password: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const updateUser = (prop, value) => setUserObject({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
    }
  });

  const validateForm = () => !Object
  .keys(user)
  .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched , true);

  const userValidators = {
    username: [
      value => value?.length >= 4 || `Username should be at least 4 letters.`, 
      value => value?.length <= 20 || `Username should be no more than 10 letters.`,
    ],
    password: [
      value => value?.length >= 4 || `Password should be at least 4 letters.`,
      value => value?.length <= 20 || `Password should be no more than 10 letters.`,
      value => /[a-zA-Z]/.test(value) || `Password should contain at least one letter.`,
      value => /[0-9]/.test(value) || `Password should contain at least one number.`,
    ],
  };

  const getValidationErrors = (prop) => {
    return userValidators[prop]
      .map(validatorFn => validatorFn(user[prop].value)) 
      .filter(value => typeof value === 'string');
  };

  const renderValidationError = prop => user[prop].touched && !user[prop].valid
        ? getValidationErrors(prop).map((error, index) => <p className="error" key={index}>{error}</p>)
        : null;


  const getClassNames = (prop) => {
    let classes = '';
    if (user[prop].touched) {
      classes += 'touched '
    }
    if (user[prop].valid) {
      classes += 'valid ';
    } else {
      classes += 'invalid ';
    }

    return classes;
  };

  const isLogin = location.pathname.includes('login');

  const login = () => {
    if (!user.username.value) {
      return alert('Invalid username!');
    }
    if (!user.password.value) {
      return alert('Invalid password!');
    }

    fetch(`${BASE_URL}/session`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username.value,
        password: user.password.value,
      }),
    })
      .then(r => r.json())
      .then(result => {
        if (result.error) {
          return alert(result.message);
        }

        try {
          const payload = jwtDecode(result.token);
          setUser(payload);
        } catch(e) {
          return alert(e.message);
        }

        localStorage.setItem('token', result.token);
        history.push('/home');
      })
      .catch(alert); 
  };

  const register = () => {
    if (!user.username.value) {
      return alert('Invalid username!');
    }
    if (!user.password.value) {
      return alert('Invalid password!');
    }

    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username.value,
        password: user.password.value,
        displayName: user.username.value,
      }),
    })
      .then(r => r.json())
      .then(r => {
        if (r.error) {
          return alert(r.message);
        }

        history.push('/login');
      })
      .catch(alert);
  };
  
  return (
    <div Nameclass="container" id="secret">
      <div className="row">
        <div className="col-sm-12"><h1>{isLogin ? 'Login' : 'Register'}</h1></div>
      </div>
      <div className="row">
        <div className="col-sm-1">
          <label htmlFor="input-username">Username:</label>
        </div>
        <div className="col-sm-11">
        <input type="text" className={getClassNames('username')} id="input-username" placeholder="username" value={user.username.value} onChange={(e) => updateUser('username', e.target.value)} /><br />
      {renderValidationError('username')}<br /><br />
        </div>
      </div>
      
      <div className="row">
      <div className="col-sm-1">
      <label htmlFor="input-password">Password:</label>
      </div>
      <div className="col-sm-11">
      <input type="password" className={getClassNames('password')} id="input-password" value={user.password.value} onChange={(e) => updateUser('password', e.target.value)} /><br />
      {renderValidationError('password')}
      </div>
      </div>
      { isLogin
        ? <Button onClick={login} disabled={validateForm()}>Login</Button>
        : <Button onClick={register} disabled={validateForm()}>Register</Button>}
    </div>
  );
};

export default SignIn;