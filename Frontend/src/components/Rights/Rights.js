import React from "react";
import "./Rights.css"

const Rights = () => {

  return (
  
    <footer class="contactinfo">
      <h2 className="ArtGallery">2021 Art Gallery Vladimir Kondarev</h2>
      <p className="copy-right" >&copy;  All Rights Reserved</p>
      <a href="https://www.linkedin.com/in/tanyo-kondarev-a799561b8/" className="creator" 
      style={{ color: "white)", fontSize: 18, fontWeight: 'bold', fontFamily: "Cambria" }}>&copy; 2021   Site Creator And Support  </a>
      
    </footer>

    
  )
}

export default Rights;

