import React, { useState } from "react";
import jwtDecode from "jwt-decode";
import Navigation from "./components/Navigation/Navigation";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import UserContext, { getLoggedUser } from "./providers/UserContext";
import SignIn from "./components/SignIn/SignIn";
import { BASE_URL } from "./common/constants";
import Home from "./components/Pages/Home/Home";
import Bio from "./components/Pages/Biography/Biography"
import GalleryContactForm from "./components/Pages/Contact/Contact";
import Gallery from "./components/Pages/Gallery/Gallery";
import NewsAndMedia from "./components/Pages/News and Media/Media"
import Oils from "./components/Pages/Gallery/Paintings/Oils"
import Watercolours from "./components/Pages/Gallery/Paintings/Watercolours"
import Drawings from "./components/Pages/Gallery/Paintings/Drawings"
import Sculptures from "./components/Pages/Gallery/Paintings/Sculptures"
import * as emailjs from "emailjs-com";
import "./App.css"


const App = () => {

  const [user, setUser] = useState(getLoggedUser());

  let payload = "";
  let loggedUser = "";
  const token = localStorage.getItem("token");
  let role = '';
  if (token) {
    payload = jwtDecode(token);
    loggedUser = payload.username;
    role = payload.role;


    fetch(`${BASE_URL}/users/${loggedUser}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token || ""}`,
      },
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.error) {
          throw new Error(data.message);
        }

      })
      .catch((error) => error.message);
  }


  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user, setUser }}>
        <div className="mainContainer">
          <Navigation />
          <div className="pageContent">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/home" exact component={Home} />
              <Route path="/login" component={SignIn} />
              <Route path="/register" component={SignIn} />
              <Route path="/bio" exact component={Bio} />
              <Route path="/contact" exact component={GalleryContactForm} />
              <Route path="/newsandmedia" exact component={NewsAndMedia} />
              <Route path="/gallery" exact component={Gallery} />
              <Route path="/oils" exact component={Oils} />
              <Route path="/watercolours" exact component={Watercolours} />
              <Route path="/drawings" exact component={Drawings} />
              <Route path="/gallery/sculptures" exact component={Sculptures} />
            </Switch>
          </div>
        </div>

      </UserContext.Provider>
    </BrowserRouter>
  );
};

export default App;

