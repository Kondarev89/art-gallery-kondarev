import { createContext } from 'react';
import JwtDecode from 'jwt-decode';
export const getLoggedUser = () =>{
  try {
    return JwtDecode(localStorage.getItem('token') || '');
  } catch (e) {
    localStorage.removeItem('token');
    return null;
  }
}

const UserContext = createContext({
  user: {},
  setUser: () => {},
});

export default UserContext;