import React from 'react';

import pv from "../../../images/pv.jpg"
import WA from "../../../images/WA.jpg"

const AboutCards = () => {


  return (<>
    <div id="AboutCont" style={{display: 'flex', flexDirection: 'row' ,background: "transparent", }} >
    <p id="para1"><h3 style={{ color: "rgb(17, 17, 17)", fontSize: 24, fontWeight: 'bold', fontFamily: "Cambria" }}>Articles about Vladimir Kondarev :</h3>
    </p>
     <li><a href="https://bnr.bg/starazagora/post/101099982/chirpanskiat-hudojnik-vladimir-kondarev-otbelazva-svoata-60-godishnina-s-interesna-i-bogata-retrospektivna-izlojba"><img src="https://d1yjjnpx0p53s8.cloudfront.net/styles/logo-original-577x577/s3/062011/bulgarian_national_radio.png?itok=kZ2SmybJ" id="aboutImg1" style={{height: "420px", width: "80%", display: 'block',flex: 1}} /></a></li>
     <li><a href="https://www.duma.bg/plasticheskite-vazdishki-na-vladimir-kondarev-n188621?h=pdfarchive&pdfarchiveId=2974"><img src={pv} id="cardImg2" style={{height: "420px", width: "80%", display: 'block',flex: 1}} /></a></li>
     <li><a href="https://duma.bg/yubileyna-izlozhba-na-vladimir-kondarev-n12919"><img src={WA} id="cardImg3" style={{height: "420px", width: "80%", display: 'block',flex: 1}} /></a></li>
     
    </div>
  </>);
};

export default AboutCards;
