import React from "react";
import AboutCards from "./AboutCards";
import v2 from "../../../images/v2.jpg"

import Rights from "../../Rights/Rights";



const About = () => {
    return (
        <div>
            <section className="section">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-8 col-md-8 mt-4 pt-4 mt-sm-2 pt-sm-2">

                            <img src={v2} style={{ height: 600, width: '100%', overflow: 'hidden' }} className="rounded img-fluid mx-auto " alt="" id="aboutImg" />

                            <div className="section-title ml-lg-4">
                                <a href="https://www.facebook.com/vladimir.kondarev.3"><h4 className="title mb-8" style={{ color: "rgb(17, 17, 17)", fontSize: 48, fontWeight: 'bold', fontFamily: "Cambria" }}>Vladimir Kondarev</h4></a>
                                <p className="text-muted" id="text" style={{ color: "black", fontSize: 24, fontFamily: "Cambria" }}>Vladimir Kondarev is a Renown Bulgarian Artist born in the year 1959 in
                                the city of Chirpan. His education starts in very early ears in his first scrool and after he is
                                accepded in The National High School of Plastic Arts and Design “Academician Dechko Uzunov”. After
                                graduation he accepts several specialisation such as sculpture, painting and graphic arts. Since 1988
                                he is the currator of the "Art Gallery - Gerogy Danchov - Zograf" in the city of Chirpan.
                                </p>
                               
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                <AboutCards />

            </section>

            <Rights />
        </div>

    )

};
export default About;